// Copyright (C) 2022 Sorbonne Université
// Copyright (C) 2018, 2020 |Meso|Star> (contact@meso-star.com)
// Copyright (C) 2018 CNRS, Université Paul Sabatier
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
:toc:

les2ecrp(1)
===========

NAME
----
les2ecrp - convert radiative properties from NetCDF to ecrp(5) file format

SYNOPSIS
--------
[verse]
*les2ecrp* [_option_] ... -i __NetCDF__

DESCRIPTION
-----------
*les2ecrp* generates a *ecrp*(5) file from radiative properties stores in a
NetCDF file [1]. 

Expected dimension in the submitted NetCDF file are:

* _nx_, _ny_ and _nz_: the number of grid celles along each of the X, Y, Z
  dimensions

* _ncomponents_: the number of components in the file

Expected variables in the submitted NetCDF file are:

* _x_scene_ *and* _y_scene_: one dimensional list of position of the
  center of each cell along the West-East and South-North horizontal axis,
  respectively. This should be a homogeneous mesh: each cell should have the
  same width along each axis. The unit is assumed to be the meter but it can
  be adjusted by the *-m* option.

* _height_: position of the center of each cell along the
  vertical axis. The vertical mesh can possibly be inhomogeneous: each cell
  can have different vertical extent. At least one of this variable must be
  defined. *les2ecrp* assumes that the vertical columns are the same for each
  cell along the West-East and South-North axis. The unit is assumed to be the
  meter but it can be adjusted by the *-m* option.

* _Extinction_: per component exctinction coefficient in each grid cell; in m^-1.

* _SS_alb_: per component single scattering albedo in each grid cell.

* _g_: per component asymetry coefficient in each cell.

OPTIONS
-------
*-c*::
  Advanced checks on the validity of the submitted _NetCDF_ file with respect
  to the *les2ecrp* prerequisites on the NetCDF data. Note that this option
  can increase significantly the conversion time.

*-f*::
  Force overwrite of the _output_ file.

*-h*::
  List short help and exit.

*-i* _NetCDF_::
  NetCDF file to convert.

*-m* _float-to-meter_::
  Scale factor from floating point unit to meters. By default it is set to 1.

*-o* _output_::
  Destination file where the *ecrp*(5) file is written. If not defined, the
  results are written to standard output.

*-p* _page-size_::
  Targeted page size in bytes; must be a power of 2. The size of the converted
  NetCDF data and their starting address into the *ecrp*(5) file are aligned
  according to _page-size_. By default, _page-size_ is 4096 bytes.

*-q*::
  Write nothing to _output_. Might be used in conjunction of the *-c* option
  to only check the submitted _NetCDF_.

*-v*::
  Display version information and exit.

EXAMPLES
--------

Convert the *properties.nc* NetCDF file. Write the resulting *ecrp*(5) file in
*properties.ecrp* excepted if the file already exists; in this case an error is
notified, the program stops and the *properties.ecrp* file remains unchanged:

    $ les2ecrp -i properties.nc -o properties.ecrp

Convert the *properties_km.nc* file to *ecrp*(5) file format.  Use the *-f* option
to write the output file *properties.ecrp* even though it already exists.  The
*properties_km.nc* file to convert has its spatial unit in kilo-meters while the
*ecrp*(5) file formats assumes meters: use the *-m 1000* option to map
kilo-meters to meters:

    $ les2ecrp -i properties_km.nc -m 1000 -o properties.ecrp

Check that the submitted *properties.nc* file is valid regarding the *les2ecrp*
constraints. Use the *-q* option to disable the generation of output data:

    $ les2ecrp -c -i properties.nc -q

NOTES
-----
1. Network Common Data Form -
<https://www.unidata.ucar.edu/software/netcdf/>

COPYRIGHT
---------
Copyright &copy; 2018, 2020 |Meso|Star> <contact@meso-star.com>.
Copyright &copy; 2018 CNRS, Université Paul Sabatier
<contact-edstar@laplace.univ-tlse.fr>.
*les2ecrp* is free software released under the GPLv3+ license: GNU GPL version
3 or later <https://gnu.org/licenses/gpl.html>. You are free to change and
redistribute it. There is NO WARRANTY, to the extent permitted by law.

SEE ALSO
--------
*ecrp*(5)
