/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ECRP_H
#define ECRP_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(ECRP_SHARED_BUILD) /* Build shared library */
  #define ECRP_API extern EXPORT_SYM
#elif defined(ECRP_STATIC) /* Use/build static library */
  #define ECRP_API extern LOCAL_SYM
#else /* Use shared library */
  #define ECRP_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the ecrp function `Func'
 * returns an error. One should use this macro on ecrp function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define ECRP(Func) ASSERT(ecrp_ ## Func == RES_OK)
#else
  #define ECRP(Func) ecrp_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data types */
struct ecrp;

struct ecrp_desc {
  size_t pagesize; /* In bytes */
  int irregular_z;

  /* #coordinates along the X, Y and Z axis. X means for the West-Est axis, Y
   * is the South-North axis and finally Z is the altitude */
  size_t spatial_definition[3];
  /* Number of components that interact with light */
  size_t ncomponents;

  double lower[3]; /* Lower position of the grid in meters */
  double upper[3]; /* Upper position of the grid in meters */
  double vxsz_x; /* Voxel size in X in meters */
  double vxsz_y; /* Voxel size in Y in meters */
  const double* vxsz_z; /* Voxel size along Z in meters */
  const double* coord_z; /* Voxel lower bound along Z. NULL if !irregular_z */

  const double* Extinction;
  const double* SS_alb;
  const double* g; /* Pressure */
};
#define ECRP_DESC_NULL__ \
  {0,-1,{0,0,0},0,{-1,-1,-1},{0,0,0},-1,-1,NULL,NULL,NULL,NULL,NULL}
static const struct ecrp_desc ECRP_DESC_NULL = ECRP_DESC_NULL__;

BEGIN_DECLS

/*******************************************************************************
 * ECRP API
 ******************************************************************************/
ECRP_API res_T
ecrp_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct ecrp** ecrp);

ECRP_API res_T
ecrp_load
  (struct ecrp* ecrp,
   const char* path);

ECRP_API res_T
ecrp_load_stream
  (struct ecrp* ecrp,
   FILE* stream);

ECRP_API res_T
ecrp_ref_get
  (struct ecrp* ecrp);

ECRP_API res_T
ecrp_ref_put
  (struct ecrp* ecrp);

ECRP_API res_T
ecrp_get_desc
  (const struct ecrp* ecrp,
   struct ecrp_desc* desc);

/* Internal function */
static FINLINE double
ecrp_dblgrid4D_at__
  (const double* grid,
   const struct ecrp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t nc)
{
  size_t row, slice, array;
  ASSERT(desc && grid);
  ASSERT(x < desc->spatial_definition[0]);
  ASSERT(y < desc->spatial_definition[1]);
  ASSERT(z < desc->spatial_definition[2]);
  ASSERT(nc < desc->ncomponents);
  row = desc->spatial_definition[0];
  slice = desc->spatial_definition[1] * row;
  array = desc->spatial_definition[2] * slice;
  return grid[nc*array + z*slice + y*row + x];
}

static FINLINE double
ecrp_desc_Extinction_at
  (const struct ecrp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t nc)
{
  return ecrp_dblgrid4D_at__(desc->Extinction, desc, x, y, z, nc);
}


static FINLINE double
ecrp_desc_SS_alb_at
  (const struct ecrp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t nc)
{
  return ecrp_dblgrid4D_at__(desc->SS_alb, desc, x, y, z, nc);
}

static FINLINE double
ecrp_desc_g_at
  (const struct ecrp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t nc)
{
  return ecrp_dblgrid4D_at__(desc->g, desc, x, y, z, nc);
}

static FINLINE void
ecrp_desc_get_voxel_aabb
  (const struct ecrp_desc* desc,
   const size_t x, const size_t y, const size_t z,
   double lower[3],
   double upper[3])
{
  ASSERT(desc && lower && upper);
  ASSERT(x < desc->spatial_definition[0]);
  ASSERT(y < desc->spatial_definition[1]);
  ASSERT(z < desc->spatial_definition[2]);
  lower[0] = (double)x * desc->vxsz_x;
  lower[1] = (double)y * desc->vxsz_y;
  upper[0] = lower[0] + desc->vxsz_x;
  upper[1] = lower[1] + desc->vxsz_y;
  if(!desc->irregular_z) {
    lower[2] = (double)z * desc->vxsz_z[0];
    upper[2] = lower[2] + desc->vxsz_z[0];
  } else {
    lower[2] = desc->coord_z[z];
    upper[2] = lower[2] + desc->vxsz_z[z];
  }
}

END_DECLS

#endif /* ECRP_H */
