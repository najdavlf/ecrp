#!/bin/bash

# Copyright (C) 2018 |Meso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */
set -e
set -o pipefail

if [ $# -lt 1 ]; then
  >&2 echo "Usage: $0 -NAME LES-NETCDF "
  exit -1
fi

dimensions=$(ncdump -h $1 | sed '/^ *variables/,$d' | sed '1,2d')
nx=$(echo $dimensions | sed -n 's/^.*nx *= *\([0-9]\+\) *;.*$/\1/p')
ny=$(echo $dimensions | sed -n 's/^.*ny *= *\([0-9]\+\) *;.*$/\1/p')
nz=$(echo $dimensions | sed -n 's/^.*nz *= *\([0-9]\+\) *;.*$/\1/p')
ncomponents=$(echo $dimensions | sed -n 's/^.*ncomponents *= *\([0-9]\+\) *;.*$/\1/p')

if [ -z "$ncomponents" ]; then
  ncomponents=$(echo $dimensions | sed -n 's/^.*ncnomponents *=.*\/\/ *(\([0-9]\+\) currently).*$/\1/p')
fi

if [ -z "$nx" -o -z "$ny" -o -z "$nz" -o -z "$ncomponents" ]; then
  >&2 echo $0: Error retrieving the dimensions of \"$1\"
  exit -1
fi

name=$(basename $1)
name=${name%.*}
{
  echo $nx
  echo $ny
  echo $nz
  echo $ncomponents
} > ${name}_desc
