# EarthCare: Radiative Properties

This project describes the ecrp binary fileformat that is used to store the
radiative properties of an atmospheric constituent. The provided `les2ecrp`
command line tool extracts these properties from a LES output stored in a
[NetCDF](https://www.unidata.ucar.edu/software/netcdf/) file, and converts them
in the `ecrp` fileformat. Finally, a library is provided to load the `ecrp`
fileformat. Since the data to load can be huge, this library silently
loads/unloads them dynamically allowing to process data that are too large to
fit in the main memory.

## How to build

The `les2ecrp` program and `ecrp` library  are compatible GNU/Linux 64-bits.
They rely on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build.  They also depend
on the [RSys](https://gitlab.com/vaplv/rsys/) library. Furthermore, the
`les2ecrp` tool depends on the
[NetCDF](https://www.unidata.ucar.edu/software/netcdf/) C library.
Both eventually depend on the [AsciiDoc](https://asciidoc.org/) suite of
tools; if available, the man pages for the reference documentation will be
generated.

To build them, first ensure that CMake is installed on your system. Then
install the RCMake package as well as the aforementioned prerequisites. Finally
generate the project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake](https://cmake.org/documentation) for further
informations on CMake.

## Copyright notice

Copyright (C) 2022 Sorbonne Université
Copyright (C) 2018, 2020, 2021 |Meso|Star> (<contact@meso-star.com>).
Copyright (C) 2018 Centre National de la Recherche Scientifique (CNRS).
Copyright (C) 2018 Université Paul Sabatier (<contact-edstar@laplace.univ-tlse.fr>)

## License

`ecrp` and `les2ecrp` are free software released under the GPL v3+ license: GNU
GPL version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
